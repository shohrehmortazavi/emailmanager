﻿using System;

namespace EmailManager.ViewModels
{
    public class EmailViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string RecieverName { get; set; }
        public string Body { get; set; }
        public bool IsTrashed { get; set; } = false;
        public DateTime CreateDate { get; set; }

    }
}
