﻿using AutoMapper;
using EmailManager.DataAccess.Models;
using EmailManager.ViewModels;

namespace EmailManager.Services
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Email, EmailViewModel>().ReverseMap();
        }
    }
}
