﻿using AutoMapper;
using EmailManager.DataAccess.Models;
using EmailManager.Repository;
using EmailManager.ServicesInterface;
using EmailManager.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailManager.Services
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepositoryAsync _emailRepository;
        private readonly IMapper _mapper;

        public EmailService(IEmailRepositoryAsync emailRepository, IMapper mapper)
        {
            _emailRepository = emailRepository;
            _mapper = mapper;
        }

        public async Task<EmailViewModel> GetById(int emailId)
        {
            var email = await _emailRepository.GetAsync(emailId);
            return _mapper.Map<EmailViewModel>(email);
        }

        public async Task<List<EmailViewModel>> GetAll()
        {
          //  var emails = await _emailRepository.GetAllAsync();
            var emails = await _emailRepository.GetAllAsync(mail=>mail.IsTrashed==false);
            return _mapper.Map<List<EmailViewModel>>(emails);
        }

        public async Task<EmailViewModel> Create(EmailViewModel email)
        {
            var emailDb = _mapper.Map<Email>(email);
            var result = await _emailRepository.AddAsync(emailDb);
            email.Id = result.Id;
            return email;
        }

        public async Task<EmailViewModel> Update(EmailViewModel email)
        {
            var emailDb = _mapper.Map<Email>(email);
            var returnEmailDb = _emailRepository.UpdateAsync(emailDb, email.Id);
            await _emailRepository.SaveAsync();
            email.Id = returnEmailDb.Id;
            return email;
        }

        public async Task<int> Delete(int emailId)
        {
            _emailRepository.DeleteAsync(emailId);
            return await _emailRepository.SaveAsync();
        }


        public async Task IsTrashed(int emailId)
        {
            var email =await _emailRepository.GetAsync(emailId);
            email.IsTrashed = true;

            var returnEmailDb = _emailRepository.UpdateAsync(email, email.Id);
            await _emailRepository.SaveAsync();
        }


    }

}
