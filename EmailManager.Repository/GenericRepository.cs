﻿using EmailManager.DataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {

        private readonly IUnitOfWork _unitOfWork;
        public GenericRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public virtual T Add(T t)
        {
            _unitOfWork.Context.Set<T>().Add(t);
            return t;
        }
        public virtual async Task<T> AddAsync(T t)
        {
            _unitOfWork.Context.Set<T>().Add(t);
            await _unitOfWork.Context.SaveChangesAsync();
            return t;
        }



        public virtual IQueryable<T> GetAll()
        {
            return _unitOfWork.Context.Set<T>();
        }
        public virtual async Task<ICollection<T>> GetAllAsync()
        {
            return await _unitOfWork.Context.Set<T>().ToListAsync();
        }




        public virtual ICollection<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return _unitOfWork.Context.Set<T>().Where(filter).ToList();

        }
        public virtual async Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>> filter)
        {
            return await _unitOfWork.Context.Set<T>().Where(filter).ToListAsync();

        }



        public virtual T Get(int id)
        {
            return _unitOfWork.Context.Set<T>().Find(id);
        }
        public virtual async Task<T> GetAsync(int id)
        {
            return await _unitOfWork.Context.Set<T>().FindAsync(id);
        }



        public virtual void Delete(int id)
        {
            var entity = _unitOfWork.Context.Set<T>().Find(id);
            _unitOfWork.Context.Set<T>().Remove(entity);
        }
        public virtual void DeleteAsync(int emailId)
        {
            var entity = _unitOfWork.Context.Set<T>().Find(emailId);
            _unitOfWork.Context.Set<T>().Remove(entity);
        }


        public virtual T Update(T t, object key)
        {

            if (t == null)
                return null;

            T exist = _unitOfWork.Context.Set<T>().Find(key);
            if (exist != null)
                _unitOfWork.Context.Entry(exist).CurrentValues.SetValues(t);

            return exist;
        }
        public virtual async Task<T> UpdateAsync(T t, object key)
        {
            if (t == null)
                return null;
            T exist = await _unitOfWork.Context.Set<T>().FindAsync(key);
            if (exist != null)
            {

                _unitOfWork.Context.Entry(exist).CurrentValues.SetValues(t);
                await _unitOfWork.Context.SaveChangesAsync();
            }
            return exist;
        }



        public virtual void Save()
        {
            _unitOfWork.Context.SaveChanges();
        }

        public virtual async Task<int> SaveAsync()
        {
            return await _unitOfWork.Context.SaveChangesAsync();
        }



        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        Task<int> IGenericRepository<T>.DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
