﻿using EmailManager.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public class EmailRepository : GenericRepository<Email>, IEmailRepositoryAsync
    {

        public EmailRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        public override Task<Email> GetAsync(int id)
        {
            return base.GetAsync(id);
        }
        public override Task<ICollection<Email>> GetAllAsync()
        {
            return base.GetAllAsync();
        }

        public override Task<ICollection<Email>> GetAllAsync(Expression<Func<Email, bool>> filter)
        {
            return base.GetAllAsync(filter);
        }

        public override Task<Email> AddAsync(Email email)
        {
            return base.AddAsync(email);
        }
        public override Task<Email> UpdateAsync(Email t, object key)
        {
            return base.UpdateAsync(t, key);
        }
        public override void DeleteAsync(int emailId)
        {
            base.DeleteAsync(emailId);
        }
        public override Task<int> SaveAsync()
        {
            return base.SaveAsync();
        }
    }
}
