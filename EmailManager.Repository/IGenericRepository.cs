﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public interface IGenericRepository<T> : IDisposable where T : class
    {

        T Add(T t);
        Task<T> AddAsync(T t);


        void Delete(int id);
        Task<int> DeleteAsync(int id);

        T Get(int id);
        Task<T> GetAsync(int id);


        IQueryable<T> GetAll();
        Task<ICollection<T>> GetAllAsync();

        ICollection<T> GetAll(Expression<Func<T, bool>> filter);
        Task<ICollection<T>> GetAllAsync(Expression<Func<T, bool>> filter);

        void Save();
        Task<int> SaveAsync();


        T Update(T t, object key);
        Task<T> UpdateAsync(T t, object key);

    }
}
