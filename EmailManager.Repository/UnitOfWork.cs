﻿using EmailManager.DataAccess;
using System.Threading;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        public EmailManagerDbContext Context { get; }

        public UnitOfWork(EmailManagerDbContext context)
        {
            Context = context;
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        public async Task<int> SaveAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return await Context.SaveChangesAsync(cancellationToken);
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}


