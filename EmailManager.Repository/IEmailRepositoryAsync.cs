﻿using EmailManager.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public interface IEmailRepositoryAsync : IGenericRepository<Email>
    {
        Task<Email> GetAsync(int id);
        Task<ICollection<Email>> GetAllAsync();
        Task<ICollection<Email>> GetAllAsync(Expression<Func<Email, bool>> filter);
        Task<Email> AddAsync(Email email);
        Task<Email> UpdateAsync(Email t, object key);
        void DeleteAsync(int emailId);
        Task<int> SaveAsync();

    }
}
