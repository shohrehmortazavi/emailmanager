﻿using EmailManager.DataAccess;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace EmailManager.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        EmailManagerDbContext Context { get; }
        void Save();
        Task<int> SaveAsync(CancellationToken cancellationToken);
    }
}
