﻿using System;

namespace EmailManager.Models
{
    public class Email
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Subject { get; set; }
        public string RecieverName { get; set; }
        public string Body { get; set; }

        [Column(TypeName = "Date")]
        public DateTime CreateDate { get; set; }

    }
}
