﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EmailManager.DataAccess.Migrations
{
    public partial class firstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Emails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Subject = table.Column<string>(maxLength: 100, nullable: false),
                    RecieverName = table.Column<string>(maxLength: 100, nullable: false),
                    Body = table.Column<string>(maxLength: 4000, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "Date", nullable: false),
                    IsTrashed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emails", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Emails");
        }
    }
}
