﻿using EmailManager.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace EmailManager.DataAccess
{
    public class EmailManagerDbContext : DbContext
    {
        public EmailManagerDbContext(DbContextOptions options) : base(options)
        { }

        public DbSet<Email> Emails { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            builder.Entity<Email>().Property(p => p.Subject).IsRequired().HasMaxLength(100);
            builder.Entity<Email>().Property(p => p.Body).IsRequired().HasMaxLength(4000);
            builder.Entity<Email>().Property(p => p.RecieverName).IsRequired().HasMaxLength(100);
         builder.Entity<Email>().Property(p => p.CreateDate).IsRequired();
        }     
    }
}
