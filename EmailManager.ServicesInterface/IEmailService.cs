﻿using EmailManager.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailManager.ServicesInterface
{
    public interface IEmailService
    {
        Task<List<EmailViewModel>> GetAll();
        Task<EmailViewModel> GetById(int peopleId);
        Task<EmailViewModel> Create(EmailViewModel email);
        Task<EmailViewModel> Update(EmailViewModel email);
        Task IsTrashed(int emailId);
        Task<int> Delete(int emailId);
    }
}
