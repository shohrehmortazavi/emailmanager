﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmailManager.ServicesInterface;
using EmailManager.ViewModels;
using Microsoft.AspNetCore.Mvc;


namespace EmailManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }


        [HttpGet]
        public async Task<List<EmailViewModel>> Get()
        {
            return await _emailService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<EmailViewModel> Get(int id)
        {
            return await _emailService.GetById(id);
        }

        [HttpPost]
        public async Task<EmailViewModel> Post(EmailViewModel email)
        {
            return await _emailService.Create(email);
        }

        [HttpPut]
        public async Task<EmailViewModel> Put(EmailViewModel email)
        {
            return await _emailService.Update(email);

        }

        [HttpDelete("{id}")]
        public async Task<int> Delete(int id)
        {
            return await _emailService.Delete(id);
        }
    }
}
